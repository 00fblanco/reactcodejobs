// Dependencies
import React, {Component} from 'react';

class Index extends Component {
    render(){
        return(
            <div className="Index">
                <h1> Index page </h1>
            </div>
        );
    }
}

export default Index;